//
//  ViewController.h
//  CountWorld
//
//  Created by Triveni Banpela on 2/3/16.
//  Copyright © 2016 Triveni Banpela. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *labelHelloWorld;

- (IBAction)ButtonClickMe:(id)sender;

@end

